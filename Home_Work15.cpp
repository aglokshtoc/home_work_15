﻿#include <iostream>

void number(int k, int show) {
    for (int i = 1; i <= k; i++) {
        if (show == 2 && i % 2 == 0) {
            std::cout << i << " ";
        }
        else if (show == 1 && i % 2 == 1) {
            std::cout << i << " ";
        }
    }
}

int main()
{
    int k;
    std::cout << "Input number: ";
    std::cin >> k;
    int show;
    std::cout << "1 - to show odd or 2 - to show even: ";
    std::cin >> show;

    number(k, show);
}